import { LogFormat, LogLevel } from 'home/common/services/logging';


export interface IEnvironment
{
    production: boolean,
    port: number,
    logLevel: LogLevel,
    logMaxFileSize: number,
    accessLogFormat: LogFormat,
    accessLogFile: string,
    accessLogFileFormat: LogFormat,
    errorLogFormat: LogFormat,
    errorLogFile: string,
    errorLogFileFormat: LogFormat,
    appLogFormat: LogFormat,
    appLogFile: string,
    appLogFileFormat: LogFormat,
    appName: string,
}
