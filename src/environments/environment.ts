import { LogFormat, LogLevel } from 'home/common/services/logging';
import { IEnvironment } from './IEnvironment';


const logLevel = process.env.LOG_LEVEL as LogLevel || 'debug';

export const environment: IEnvironment = {
    production         : false,
    port               : Number(process.env.port) || 3000,
    logLevel           : logLevel,
    logMaxFileSize     : Number(process.env.LOG_MAX_FILESIZE) || 1024 * 1024 * 1024 * 2,
    appName            : process.env.APP_NAME || 'express-app',
    appLogFormat       : process.env.APP_LOG_FORMAT as LogFormat || `custom`,
    appLogFile         : process.env.APP_LOG_FILEPATH || `app.${logLevel}.log`,
    appLogFileFormat   : process.env.APP_LOG_FILEFORMAT as LogFormat || `simple`,
    accessLogFormat    : process.env.ACCESS_LOG_FORMAT as LogFormat || 'custom',
    accessLogFile      : process.env.ACCESS_LOG_FILEPATH || `access.${logLevel}.log`,
    accessLogFileFormat: process.env.ACCESS_LOG_FILEFORMAT as LogFormat || 'simple',
    errorLogFormat     : process.env.ERROR_LOG_FORMAT as LogFormat || 'custom',
    errorLogFile       : process.env.ERROR_LOG_FILEPATH || `error.${logLevel}.log`,
    errorLogFileFormat : process.env.ERROR_LOG_FILEFORMAT as LogFormat || 'simple',
};
