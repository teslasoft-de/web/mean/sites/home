import commandLineArgs from 'command-line-args';
import dotenv from 'dotenv';
import path from 'path';

// Setup command line options
const options = commandLineArgs([
    {
        name        : 'env',
        alias       : 'e',
        defaultValue: 'development',
        type        : String,
    },
]);

// Read environment configuration using dotenv
const configOutput = dotenv.config({
    path: path.join(__dirname, `env/${options.env}.env`),
});

if (configOutput.error)
    throw configOutput.error;
