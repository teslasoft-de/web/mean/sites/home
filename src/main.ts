import 'config';
import { server } from 'home';
import appLogger from 'home/common/services/logging';


server.on('error', ex => appLogger.exception(ex));
