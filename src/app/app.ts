import bodyParser from 'body-parser';
import compassSourcemaps from 'apps/home/src/compass/sourcemaps';
import compression from 'compression';
import 'config';
import { environment } from 'environment';
import errorHandler from 'errorhandler';
import express from 'express';
import { glob } from 'glob';
import { AppBase } from 'home/common/model/app';
import { accessFileLogger, accessLogger, errorFileLogger, errorLogger } from 'home/common/services/logging';
import webSecurity from 'home/common/services/webSecurity';
import path from 'path';


const app = express();

app.set('port', environment.port);

app.use(accessLogger);
app.use(accessFileLogger);

// Access logging and security
switch (process.env.NODE_ENV) {
    case 'development':
        app.use(compassSourcemaps('/apps/home/src/assets/css/'));
        break;
    case 'production':
        app.use(webSecurity);
        break;
    default:
        throw `Unknown app environment '${process.env.NODE_ENV}'.`;
}

// Document Root
app.use(express.static(path.join(__dirname, 'assets'), { maxAge: 31557600000 }));

// View Config
app.set('views', glob.sync(`**/view`));
app.set('view engine', 'pug');
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Routing
AppBase.Instance.Modules.forEach(module => module.Router && app.use(module.Router));

// Error Handling
app.use(errorLogger);
app.use(errorFileLogger);

// Error Response Handling
if (process.env.NODE_ENV === 'development')
    app.use(errorHandler());

export default app;
