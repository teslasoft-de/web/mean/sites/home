import { environment } from 'environment';
import expressWinston from 'express-winston';
import { express_winston_config, winston_format, winston_transport } from 'home/common/services/logging/logger';
import path from 'path';


export const accessLogger = expressWinston.logger(
    express_winston_config(
        environment.accessLogFormat,
        winston_transport(
            'console',
            environment.logLevel,
        ),
    ),
);

export const accessFileLogger = expressWinston.logger(
    express_winston_config(
        environment.accessLogFileFormat,
        winston_transport(
            'file',
            environment.logLevel,
            path.join(__dirname, environment.accessLogFile),
            {
                maxsize: environment.logMaxFileSize,
            },
        ),
    ),
);
