import { environment } from 'environment';
import { winston_config, winston_format, winston_transport } from 'home/common/services/logging/logger';
import path from 'path';
import winston from 'winston';


export interface winstonLogger extends winston.Logger
{
    exception: (ex: Error) => winstonLogger;
}

const appLogger: winstonLogger = winston.createLogger(winston_config(
    environment.appName,
    winston_transport(
        'console',
        environment.logLevel,
        null,
        {
            format: winston_format(environment.appLogFormat),
        },
    ),
    winston_transport(
        'file',
        environment.logLevel,
        path.join(__dirname, environment.appLogFile),
        {
            maxsize: environment.logMaxFileSize,
            format : winston_format(environment.appLogFileFormat),
        },
    ),
)) as winstonLogger;

// Monkey patching Winston because it incorrectly logs `Error` instances even in 2021
// Related issue: https://github.com/winstonjs/winston/issues/1498
appLogger.exception = function(this: winstonLogger, ex: Error)
{
    appLogger.error(`${ex}\n${ex.stack}`, { error: ex });

    return this;
};

if (process.env.NODE_ENV !== 'production') {
    appLogger.debug(`Logging initialized at '${environment.logLevel}' level`);
}

export default appLogger;
