import { environment } from 'environment';
import expressWinston from 'express-winston';
import { express_winston_config, winston_transport } from 'home/common/services/logging/logger';
import path from 'path';


export const errorLogger = expressWinston.errorLogger(
    express_winston_config(
        environment.errorLogFormat,
        winston_transport(
            'console',
            environment.logLevel,
        ),
    ),
);

export const errorFileLogger = expressWinston.errorLogger(
    express_winston_config(
        environment.errorLogFileFormat,
        winston_transport(
            'file',
            environment.logLevel,
            path.join(__dirname, environment.errorLogFile),
            {
                maxsize: environment.logMaxFileSize,
            },
        ),
    ),
);

