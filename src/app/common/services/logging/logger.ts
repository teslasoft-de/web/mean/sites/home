import { environment } from 'environment';
import { LoggerOptions } from 'express-winston';
import { Format, TransformableInfo } from 'logform';
import winston from 'winston';
import * as TransportStream from 'winston-transport';
import { ConsoleTransportOptions, FileTransportOptions } from 'winston/lib/winston/transports';


export type LogFormat = 'simple' | 'json' | 'custom' | 'meta' | 'error';

const logMessageTemplate = (meta = false, pretty = false) => (info: TransformableInfo) =>
{
    let message = `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
    // Append meta data to file transports only
    if (meta && info.meta)
        message += ` ${JSON.stringify(info.meta, null, pretty ? 2 : 0)}`;
    return message;
};

/**
 * Returns a winston transport format configuration object.
 * @param {"simple" | "json" | "cli" | "custom"} format
 * @param templateFunction
 * @returns {Format}
 */
export function winston_format(
    format: LogFormat,
    templateFunction = logMessageTemplate): Format
{
    switch (format) {
        case 'json':
            return winston.format.combine(
                winston.format.metadata(),
                winston.format.json(),
            );
        case 'simple':
            return winston.format.combine(
                winston.format.timestamp(),
                winston.format.label({ label: environment.appName }),
                winston.format.printf(templateFunction(true)),
            );
        case 'custom':
            return winston.format.combine(
                winston.format.timestamp(),
                winston.format.label({ label: environment.appName }),
                winston.format.colorize(),
                winston.format.align(),
                winston.format.printf(templateFunction()),
            );
        case 'meta':
            return winston.format.combine(
                winston.format.timestamp(),
                winston.format.label({ label: environment.appName }),
                winston.format.colorize(),
                winston.format.align(),
                winston.format.printf(templateFunction(true, true)),
            );
        default:
            throw `Unknown log transport format parameter value '${format}'.`;
    }
}

export type LogLevel = 'silly' | 'debug' | 'verbose' | 'http' | 'info' | 'warn' | 'error';

/**
 * Returns a winston transport configuration object.
 * @param {"console" | "file"} method The transport method to be used.
 * @param level The winston log levels (silly, debug, verbose, http, info, warn or error).
 * @param fileName The log file name (necessary if method is 'file').
 * @param options Optional file transport options.
 * @param format
 * @returns {TransportStream}
 */
export function winston_transport(
    method: 'console' | 'file',
    level: LogLevel = 'debug',
    fileName        = 'debug.log',
    options?: ConsoleTransportOptions | FileTransportOptions): TransportStream
{
    let transportOptions: FileTransportOptions;

    switch (method) {
        case 'console':
            return new winston.transports.Console({
                level,
                ...options,
            });
        case 'file':
            transportOptions = {
                filename: `${fileName}`,
                level,
                ...options,
            };

            return new winston.transports.File(transportOptions);
        default:
            throw `Unknown log transport method parameter value '${method}'.`;
    }
}

/**
 * Returns a winston configuration object using the specified service name as.
 * @param {string} serviceName
 * @param {TransportStream} transports
 * @returns {winston.LoggerOptions}
 */
export function winston_config(serviceName: string, ...transports: Array<TransportStream>): winston.LoggerOptions
{
    return {
        transports,
        defaultMeta     : { service: serviceName },
        handleExceptions: true,
    };
}

/**
 * Returns a winston configuration object.
 * @param {LogFormat} format
 * @param {TransportStream} transports
 * @returns {LoggerOptions}
 */
export function express_winston_config(format: LogFormat, ...transports: Array<TransportStream>): LoggerOptions
{
    return {
        format       : winston_format(format, (meta = false, pretty = false) => info =>
        {
            // Append meta data to file transports only
            return logMessageTemplate(
                meta || transports.some(transport => transport instanceof winston.transports.File),
                format == 'meta',
            )(info);
        }),
        transports,
        meta         : true,
        msg          : 'HTTP {{req.method}} {{req.url}}', // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
        expressFormat: true,
        colorize     : format == 'custom',
        ignoreRoute  : function(req, res) { return false; },
    };
}
