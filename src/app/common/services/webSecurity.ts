import lusca from 'lusca';


const webSecurity = lusca({
    csp          : {
        policy: {
            'default-src': '\'self\'',
            'img-src'    : '*',
        },
    },
    hsts         : {
        maxAge           : 31557600000,
        includeSubDomains: true,
    },
    xframe       : 'SAMEORIGIN',
    xssProtection: true,
    nosniff      : true,
});

export default webSecurity;
