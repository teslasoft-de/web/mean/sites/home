import { AppBase } from 'home/common/model/app/AppBase';
import { ModuleBase } from 'home/common/model/app/ModuleBase';
import { Constructor } from 'type-fest';


export type IModuleConfig = {
    app: Constructor<AppBase>,
    modules?: Array<Constructor<ModuleBase>>
};

const moduleConfig = new Map<Constructor<ModuleBase>, IModuleConfig>();

export function Module<T extends ModuleBase>(config?: IModuleConfig)
{
    return <Type extends Constructor<T>>(module: Type) =>
    {
        moduleConfig.set(module, config);

        return module as Type;
    };
}

export default moduleConfig;
