import { IRouter } from 'express-serve-static-core';


export abstract class ModuleBase
{
    private readonly _Modules: Array<ModuleBase>;

    public get Name(): string
    {
        return this._Name;
    }

    public get Router(): IRouter | null
    {
        return this._Router;
    }

    public get Modules(): Array<ModuleBase>
    {
        return this._Modules;
    }

    protected constructor(
        private readonly _Name: string,
        private readonly _Router?: IRouter,
        ...modules: Array<ModuleBase>
    )
    {
        this._Modules = modules;
    }

    /**
     * Called during module initialization.
     * Override to handle module dependency and service initialization.
     * @protected
     */
    protected Init?();

    public static Init(module: ModuleBase): void
    {
        module._Modules.forEach(module => module.Init && module.Init());
        module.Init && module.Init();
    }

    /**
     * Called after the application ({@link AppBase.Instance}) and all modules have been initialized.
     * Override to handle module loading.
     * @protected
     */
    protected Load?();

    public static Load(module: ModuleBase)
    {
        module._Modules.forEach(module => module.Load && module.Load());
        module.Load && module.Load();
    }
}
