import { environment } from 'environment';
import appConfig from 'home/common/model/app/App.decorator';
import moduleConfig, { Module } from 'home/common/model/app/Module.decorator';
import { ModuleBase } from 'home/common/model/app/ModuleBase';
import appLogger from 'home/common/services/logging';
import { Constructor, PackageJson } from 'type-fest';


/**
 * Base class for the application master state.
 */
export abstract class AppBase
{
    public get Package(): PackageJson
    {
        return this._Package;
    }

    public get Modules(): ModuleBase[]
    {
        return this._Modules;
    }

    private readonly _Modules: Array<ModuleBase>;

    protected constructor(
        private readonly _Package: PackageJson,
        ..._Modules: Array<ModuleBase>
    )
    {
        this._Modules = _Modules;

        AppBase.Init(this);
    }

    /**
     * Called during application startup after all modules have been initialized.
     * Override to handle application dependency and service initialization.
     * @protected
     */
    protected Init?(): void;

    private static Init(app: AppBase): void
    {
        const sumModules = (module: ModuleBase, loaded = true) =>
            loaded && module == null
                ? 0
                : module?.Modules.map(module => sumModules(module, loaded))
                .reduce((prev, next) => prev + next, 1) || 1;

        const countModules = (initialized = true) =>
            app._Modules.map(module => sumModules(module, initialized))
                .reduce((prev, next) => prev + next, 0);

        const allModules    = countModules(false);
        const loadedModules = countModules();
        const message       = `${loadedModules} of ${allModules} modules loaded`;

        if (allModules != loadedModules)
            appLogger.warn(message);
        else {
            app._Modules.forEach(module => ModuleBase.Init(module));
            appLogger.info(message);
            app.Init && app.Init();
        }
    }

    /**
     * Called after all modules have been loaded.
     * Override to handle application loading.
     * @protected
     */
    protected Load?(): void;

    /**
     * Loads and returns the app instance defined by the {@link Module} decorator of the provided baseModule
     * parameter.
     * @param {PackageJson} packageJson The node package configuration.
     * @param {Constructor<ModuleBase>} baseModule The base module which configures the application ({@link IModuleConfig.app}).
     * @returns {T}
     */
    public static Load<T extends AppBase>(packageJson: PackageJson, baseModule: Constructor<ModuleBase>): T
    {
        const moduleConfiguration = moduleConfig.get(baseModule);
        const appConfiguration    = appConfig.get(moduleConfiguration.app);

        appLogger.info(`Loading app using environment configuration:`, { meta: environment });

        const loadModule = (module: Constructor<ModuleBase>) =>
        {
            const moduleConf = moduleConfig.get(module);
            let modules      = new Array<ModuleBase>();
            if (moduleConf?.modules?.length > 0)
                modules = moduleConf.modules.map(loadModule);

            appLogger.info(`Loading module '${module.name}'`);

            let instance: ModuleBase;
            try {
                instance = new module(...modules);
            }
            catch (ex) {
                appLogger.exception(ex);
            }

            return instance;
        };

        const baseDependencies = [loadModule(baseModule)];
        const appDependencies  = appConfiguration.modules?.map(loadModule);
        const modules          = appDependencies?.length > 0 ? baseDependencies.concat(appDependencies) : baseDependencies;
        const appInstance      = AppBase.#_Instance = new moduleConfiguration.app(packageJson, ...modules) as T;

        if (!appInstance._Modules.some(module => module == null)) {
            appInstance._Modules.forEach(module => ModuleBase.Load(module));
            appInstance.Load && appInstance.Load();
        }

        return appInstance;
    }

    static #_Instance: AppBase;

    /**
     * Returns the app instance.
     * @access This property is accessible after the application has been initialized
     * @see {@link ModuleBase.Load}
     * @returns {AppBase}
     */
    public static get Instance(): AppBase
    {
        return AppBase.#_Instance;
    }
}
