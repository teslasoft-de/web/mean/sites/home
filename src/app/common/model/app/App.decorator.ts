import { AppBase } from 'home/common/model/app/AppBase';
import { ModuleBase } from 'home/common/model/app/ModuleBase';
import { Constructor } from 'type-fest';


export type IAppConfig = {
    modules?: Array<Constructor<ModuleBase>>
};

const appConfig = new Map<Constructor<AppBase>, IAppConfig>();

/**
 * The application master state.
 * @param {IAppConfig} config The app configuration containing dependent modules.
 * @returns {<Type extends Constructor<T>>(app: Type) => Type}
 * @constructor
 */
export function App<T extends AppBase>(config: IAppConfig)
{
    return <Type extends Constructor<T>>(app: Type) =>
    {
        appConfig.set(app, config);
        return app as Type;
    };
}

export default appConfig;
