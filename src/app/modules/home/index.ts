import { Module, ModuleBase } from 'home/common/model/app';
import homeRouter from 'home/modules/home/routes';

@Module()
export default class HomeModule extends ModuleBase
{
    constructor(...modules: Array<ModuleBase>)
    {
        super('Home', homeRouter, ...modules);
    }
}
