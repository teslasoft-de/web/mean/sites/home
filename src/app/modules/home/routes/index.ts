import { Router } from 'express';
import getHome from 'home/modules/home/controller/getHome';


const homeRouter = Router();

homeRouter.get('/', getHome);

export default homeRouter
