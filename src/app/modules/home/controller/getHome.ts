import { Request, Response } from 'express';
import dashboardItems from 'home/modules/home/model/dashboardItems';


export default function(req: Request, res: Response)
{
    res.render('home', {
        title: 'Teslasoft - Home',
        dashboardItems: dashboardItems
    });
};
