type IDashboardItem = {
    size: number,
    title: string,
    glyphicon: string,
    content: string
};
const dashboardItems: IDashboardItem[] = [];
dashboardItems.push({
    size     : 12,
    title    : 'Consumer Software',
    glyphicon: 'fa fa-cubes fa-fw',
    content  : 'Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.',
});
dashboardItems.push({
    size     : 4,
    title    : 'Performance Marketing',
    glyphicon: 'fa fa-envelope fa-fw',
    content  : 'Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.',
});
dashboardItems.push({
    size     : 4,
    title    : 'Data Query Systems',
    glyphicon: 'fa fa-database fa-fw',
    content  : 'Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.',
});
dashboardItems.push({
    size     : 4,
    title    : 'IT-Consulting',
    glyphicon: 'fa fa-users fa-fw',
    content  : 'Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.',
});
dashboardItems.push({
    size     : 4,
    title    : 'Web-Hosting',
    glyphicon: 'fa fa-cogs fa-fw',
    content  : 'Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.',
});
dashboardItems.push({
    size     : 8,
    title    : 'Coming Soon - IT Hardware Support Networking',
    glyphicon: 'fa fa-share-alt fa-fw',
    content  : 'Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus..',
});

export default dashboardItems;
