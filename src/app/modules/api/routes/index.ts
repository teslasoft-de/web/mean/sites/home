import { Router } from 'express';
import getApi from 'home/modules/api/controller/getApi';


const apiRouter = Router();

apiRouter.get('/api', getApi);

export default apiRouter;
