import { Module, ModuleBase } from 'home/common/model/app';
import apiRouter from 'home/modules/api/routes';


@Module()
export default class ApiModule extends ModuleBase
{
    constructor(...modules: Array<ModuleBase>)
    {
        super('Api', apiRouter, ...modules);
    }
}
