/* eslint-disable @typescript-eslint/no-var-requires */
import { BaseModule } from 'home/base';
import { AppBase } from 'home/common/model/app';


export const app    = AppBase.Load(require('package.json'), BaseModule);
export const server = require('home/server').default;
