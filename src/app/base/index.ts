import { Teslasoft } from 'home/base/model/Teslasoft';
import { AppBase, Module, ModuleBase } from 'home/common/model/app';


@Module({
    app: Teslasoft,
})
export class BaseModule extends ModuleBase
{
    constructor(...modules: Array<ModuleBase>)
    {
        super('Teslasoft Home', null, ...modules);
    }
}

