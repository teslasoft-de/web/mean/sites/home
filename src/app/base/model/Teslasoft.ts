import { App, AppBase, ModuleBase } from 'home/common/model/app';
import appLogger from 'home/common/services/logging';

import ApiModule from 'home/modules/api';
import HomeModule from 'home/modules/home';


@App({
    modules: [
        ApiModule,
        HomeModule,
    ],
})
export class Teslasoft extends AppBase
{
    public constructor(packageJson: import('type-fest').PackageJson, ...modules: Array<ModuleBase>)
    {
        super(packageJson, ...modules);
    }

    protected Load(): void
    {
        appLogger.info(`App '${this.Modules[0].Name}' loaded`, { meta: Teslasoft.Instance });
    }
}
